# Consistant equations

Illustration for 4 famous equations:
- Boltzmann's Entropy
	$$S = k_{\rm B}\,\ln{W}$$
- Einstein's General Relativity
	$$G_{\mu\nu} + \Lambda g_{\mu\nu} = \dfrac{8\pi G}{c^4} T_{\mu\nu}$$
- Euler's Identity
	$$\mathrm{e}^{\rm i\pi} + 1 = 0$$
- Schrodinger's Equation
	$$\mathrm{i}\hbar\dfrac{\partial\,\Psi}{\partial t} = \hat{\rm H}\Psi$$

This is a adpatation on $\LaTeX$ of the background illustration of the YT-Channel [Up and Atom](https://youtube.com/c/UpandAtom).

*Note:* for the schrodinger one, you will need to put the `pgf` and the `tex` files on the same path.
